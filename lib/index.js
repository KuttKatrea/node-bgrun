#!/usr/bin/env node

var child_process = require('child_process')
var util = require('util')

if (process.argv.length > 2 ) {
    var command = process.argv[2]
    var command_args = Array.prototype.slice.apply(process.argv, [3])
} else {
    util.error("No se indicó un comando")
    process.exit(-1)
}

util.log("Launching [" + command + "] with args [" + command_args + "]")

var command_options = {
    stdio: 'ignore',
    env: process.env,
    detached: true
}

var child = child_process.spawn(command, command_args, command_options)

child.unref()

process.exit(0)